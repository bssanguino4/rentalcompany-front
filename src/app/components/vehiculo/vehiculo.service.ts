import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IVehiculo } from './interface/vehiculo';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { createRequestParams } from '../utils/request.utils';

@Injectable({
  providedIn: 'root'
})
export class VehiculoService {

  constructor(private http: HttpClient) { }


  public query(req?:any): Observable<IVehiculo[]>{
    let params = createRequestParams(req);
    return this.http.get<IVehiculo[]>(`${environment.END_POINT_VEHICULO}/api/vehiculos`, {params: params})
    .pipe(map(res => {
      return res;
    }))
  }

  public saveClient(vehiculo: IVehiculo):Observable<IVehiculo>{
    return this.http.post<IVehiculo>(`${environment.END_POINT_VEHICULO}/api/vehiculos`, vehiculo).pipe(map(res =>{
    console.warn('datos',res);
    return res;
    }));
  }
}
