import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VehiculoRoutingModule } from './vehiculo-routing.module';
import { CreateVehiculoComponent } from './create-vehiculo/create-vehiculo.component';
import { ListVehiculoComponent } from './list-vehiculo/list-vehiculo.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [CreateVehiculoComponent, ListVehiculoComponent],
  imports: [
    CommonModule,
    VehiculoRoutingModule,
    ReactiveFormsModule
  ]
})
export class VehiculoModule { }
