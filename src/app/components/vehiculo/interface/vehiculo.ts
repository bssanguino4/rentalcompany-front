import { IClients } from '../../clients/interface/clients';

export interface IVehiculo {
            id?: number,
            fechaAlquiler?: string,
            fechaDevolucion?:string ,
            numeroPlaca?: string,
            valorAlquiler?: string ,
            cliente?: IClients
               
}
