import { Component, OnInit } from '@angular/core';
import { IVehiculo } from '../interface/vehiculo';
import { VehiculoService } from '../vehiculo.service';

@Component({
  selector: 'app-list-vehiculo',
  templateUrl: './list-vehiculo.component.html',
  styleUrls: ['./list-vehiculo.component.styl']
})
export class ListVehiculoComponent implements OnInit {
vehiculo: IVehiculo[];
public vehiculoList: IVehiculo[];
pageSize= 10;
pageNumber = 0;

listPages = [];

  constructor(private vehiculoService: VehiculoService) { }

  ngOnInit() {
    this.initPagination(this.pageNumber);
  }


  initPagination(page: number): void{
    this.vehiculoService.query({
      'pageSize': this.pageSize,
      'pageNumber': page
    })
    .subscribe((res: any)=> {
      console.warn('DATOS ',res);
      this.vehiculoList = res.content;
      this.formatPage(res.totalPAges);
    })
  }


  private formatPage(countPages: number): void{
    this.listPages = [];
    for(let i = 0; i <countPages; i++){
      this.listPages.push(i);
    }
  }

}
