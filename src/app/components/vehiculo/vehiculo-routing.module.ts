import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateVehiculoComponent } from './create-vehiculo/create-vehiculo.component';
import { ListVehiculoComponent } from './list-vehiculo/list-vehiculo.component';


const routes: Routes = [
  {
    path:'create-vehiculo',
    component:CreateVehiculoComponent
  },
  {
    path:'list-vehiculo',
    component: ListVehiculoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiculoRoutingModule { }
