import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientsCreateComponent } from './clients-create/clients-create.component';
import { ClientsListComponent } from './clients-list/clients-list.component';


const routes: Routes = [
  {
    path:'clients-create',
    component: ClientsCreateComponent
  },
  {
    path:'clients-list',
    component:ClientsListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }
