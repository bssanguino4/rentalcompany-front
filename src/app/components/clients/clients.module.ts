import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientsRoutingModule } from './clients-routing.module';
import { ClientsCreateComponent } from './clients-create/clients-create.component';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ClientsCreateComponent, ClientsListComponent],
  imports: [
    CommonModule,
    ClientsRoutingModule,
    ReactiveFormsModule
  ]
})
export class ClientsModule { }
