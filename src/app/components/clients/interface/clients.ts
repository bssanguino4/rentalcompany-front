export interface IClients {
    id?: number,
    nombre?: string,
    telefono?: number,
    direccion?: string,
    email?: string,
    numeroDocumento?: number
}
