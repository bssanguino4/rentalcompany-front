import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ClientsService } from '../clients.service';

@Component({
  selector: 'app-clients-create',
  templateUrl: './clients-create.component.html',
  styleUrls: ['./clients-create.component.styl']
})
export class ClientsCreateComponent implements OnInit {
  clientFormGroup: FormGroup;
  constructor(private fB: FormBuilder, private clienteService: ClientsService) { 
    this.clientFormGroup = this.fB.group({
    id:['25'],
     nombre: ['', Validators.compose([ Validators.required])],
    telefono: ['', Validators.compose([ Validators.required])],
    direccion: ['', Validators.compose([ Validators.required])],
    email: ['', Validators.compose([ Validators.required])],
    numeroDocumento: ['', Validators.compose([ Validators.required])]
    })
  }

  ngOnInit() {
  }

  saveClient(){
    console.warn('Datos ',this.clientFormGroup.value);
    this.clienteService.saveClient(this.clientFormGroup.value)
    .subscribe(res => {
      console.warn('SAVE OK ', res);
    
    }, error => {
      console.error('ERROR ', error);
    });
    
  }

}
