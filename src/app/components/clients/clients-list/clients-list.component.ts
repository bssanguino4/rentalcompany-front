import { Component, OnInit } from '@angular/core';
import { IClients } from '../interface/clients';
import { ClientsService } from '../clients.service';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.styl']
})
export class ClientsListComponent implements OnInit {
client: IClients[];

public clientList: IClients[];
pageSize= 10;
pageNumber = 0;

listPages = [];
  constructor(private clientService: ClientsService) { }

  ngOnInit() {
    this.initPagination(this.pageNumber);
  }

  initPagination(page: number): void{
    this.clientService.query({
      'pageSize': this.pageSize,
      'pageNumber': page
    })
    .subscribe((res: any)=> {
      console.warn('DATOS ',res);
      this.clientList = res.content;
      this.formatPage(res.totalPAges);
    })
  }

  private formatPage(countPages: number): void{
    this.listPages = [];
    for(let i = 0; i <countPages; i++){
      this.listPages.push(i);
    }
  }

}
