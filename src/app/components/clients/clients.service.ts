import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IClients } from './interface/clients';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators'
import { createRequestParams } from '../utils/request.utils';
@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  constructor(private http:HttpClient) { }

  public query(req?:any): Observable<IClients[]>{
    let params = createRequestParams(req);
    return this.http.get<IClients[]>(`${environment.END_POINT_CLIENTE}/api/clients`, {params: params})
    .pipe(map(res => {
      return res;
    }))
  }


  public saveClient(client: IClients):Observable<IClients>{
    return this.http.post<IClients>(`${environment.END_POINT_CLIENTE}/api/clients`, client).pipe(map(res =>{
    console.warn('datos',res);
      
    return res;
    }));
  }
}
