import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';


const routes: Routes = [
  {
    path:'',
    component: MainComponent,
    children:[
      {
        path:'clients',
        loadChildren:() => import('../clients/clients.module')
        .then(m => m.ClientsModule)
      },
      {
        path:'vehiculo',
        loadChildren:() => import('../vehiculo/vehiculo.module')
        .then(m => m.VehiculoModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
